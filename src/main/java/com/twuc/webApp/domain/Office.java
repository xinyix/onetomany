package com.twuc.webApp.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Office {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column
  private String name;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "office_id", nullable = false, updatable = false)
  private List<Staff> staffs = new ArrayList<>();

  public Office() {
  }

  public Office(String name) {
    this.name = name;
  }

  public void add(Staff staff) {
    staffs.add(staff);
  }

  public List<Staff> getStaffs() {
    return staffs;
  }

  public void setStaffs(List<Staff> staffs) {
    this.staffs = staffs;
  }
}
