package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity
public class Staff {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column
  private String name;

  public Staff() {
  }

  public Staff(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
