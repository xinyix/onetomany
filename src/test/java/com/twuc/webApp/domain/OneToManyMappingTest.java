package com.twuc.webApp.domain;

import com.twuc.webApp.repository.OfficeRepository;
import com.twuc.webApp.repository.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class OneToManyMappingTest {

  @Autowired
  private OfficeRepository officeRepository;

  @Autowired
  private StaffRepository staffRepository;

  @Autowired
  private EntityManager entityManager;

  @Test
  void should_create_office_and_staff() {
    Office office = new Office("office");
    Staff staff = new Staff("staff");

    office.add(staff);

    officeRepository.saveAndFlush(office);
    entityManager.clear();

    Staff staffFound = staffRepository.findById(1L).orElseThrow(NoSuchElementException::new);
    assertEquals("staff", staffFound.getName());
  }

  @Test
  void should_delete_staff_when_delete_office() {
    Office office = new Office("office");
    Staff staff = new Staff("staff");

    office.add(staff);

    officeRepository.saveAndFlush(office);
    entityManager.clear();

    officeRepository.deleteById(1L);

    Optional<Staff> staffFound = staffRepository.findById(1L);
    assertFalse(staffFound.isPresent());
  }

  @Test
  void should_delete_staff_when_remove_from_office_object() {
    Office office = new Office("office");
    Staff staff = new Staff("staff");

    office.add(staff);

    officeRepository.saveAndFlush(office);
    entityManager.clear();

    Office officeFound = officeRepository.findById(1L).orElseThrow(NoSuchElementException::new);
    officeFound.getStaffs().remove(0);
    entityManager.flush();

    Optional<Staff> staffFound = staffRepository.findById(1L);
    assertFalse(staffFound.isPresent());
  }

  @Test
  void should_delete_staff_one_by_one_when_remove_from_office_object() {
    Office office = new Office("office");

    office.add(new Staff("staff"));
    office.add(new Staff("staff"));
    office.add(new Staff("staff"));
    office.add(new Staff("staff"));

    officeRepository.saveAndFlush(office);
    entityManager.clear();

    Office officeFound = officeRepository.findById(1L).orElseThrow(NoSuchElementException::new);
    officeFound.getStaffs().clear();
    entityManager.flush();

  }
}
